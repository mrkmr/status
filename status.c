#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "lib/battery.h"
#include "lib/date.h"

void print_battery (void);

int main (void)
{
	const time_t now = time(NULL);

	date_date();
	print_battery();
	printf("\n");

	exit(0);
}

void print_battery (void)
{
	char *battery_one;
	char *battery_two;

	battery_one = battery('0');
	battery_two = battery('1');

	printf("%s %s", battery_one, battery_two);

	free(battery_one);
	free(battery_two);
}
