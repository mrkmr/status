/* safely malloc's, exits on malloc failure
 *   size: the size to malloc
 */
void *safe_malloc (size_t size);

/* safely realloc's, exits on realloc failure
 *   pointer: the pointer to realloc
 *   size: the size to realloc
 */
void *safe_realloc (void *pointer, size_t size);

/* returns the contents of a file.
 *   stream: the file to return the contents of.
 */
char *read_file (FILE *stream);
