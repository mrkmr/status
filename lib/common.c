#include <stdio.h>
#include <stdlib.h>

#include "common.h"

char *read_file (FILE *stream)
{
	char *contents;

	int i;
	char c;

	i = 1;
	contents = safe_malloc(i);
	while ((c = fgetc(stream)) != EOF && (c != '\n')) {
		contents = safe_realloc(contents, i);
		contents[i - 1] = c;
		i++;
	}
	contents = realloc(contents, i);
	contents[i - 1] = '\0';

	return contents;
}

void *safe_malloc (size_t size)
{
	void *tmp;

	tmp = malloc(size);

	/* quit if the malloc fails */
	if (tmp == NULL) {
		fprintf(stderr, "malloc failed\n");
		exit(1);
	}

	return tmp;
}

void *safe_realloc (void *pointer, size_t size)
{
	pointer = realloc(pointer, size);

	/* quit if realloc fails */
	if (pointer == NULL) {
		fprintf(stderr, "realloc failed\n");
		exit(1);
	}

	return pointer;
}
