#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "battery.h"

#define _XOPEN_SOURCE 500

#define BAT_PATH_NUM_INDEX 27 /* the position at which to modify the BAT num */
#define BAT_CAPACITY_PATH "/sys/class/power_supply/BAT#/capacity"
#define BAT_STATUS_PATH   "/sys/class/power_supply/BAT#/status"

#define BAT_CHARGE    "Charging"
#define BAT_DISCHARGE "Discharging"


char *battery (char b_num)
{
	/* the contents of the battery files in /sys/ */
	char *status;
	char *capacity;
	/* the sign corresponding to status */
	char sign;
	/* the final string returned: sign + capacity */
	char *out;

	capacity = battery_get_capacity(b_num);
	status = battery_get_status(b_num);
	/* find the sign corresponding to status */
	if (strncmp(status, BAT_DISCHARGE, strlen(BAT_DISCHARGE)) == 0)
		sign = '-';
	else if (strncmp(status, BAT_CHARGE, strlen(BAT_CHARGE)) == 0)
		sign = '+';
	else  /* unknown or discharging */
		sign = '=';

	free(status);

	/* 1 for sign and 1 more for null byte */
	out = malloc(1 + strlen(capacity) + 1);

	strcpy(out, &sign);
	strcat(out, capacity);
	free(capacity);

	return out;
}

char *battery_get_capacity (int b_num)
{
	char *pcapacity; /* modified path for capacity file */
	FILE *fcapacity; /* file handle for capacity file */
	char *capacity;  /* contents of capacity file */

	/* setup files for reading capacity */
	pcapacity = strdup(BAT_CAPACITY_PATH);
	/* fail if strdup fails */
	if (pcapacity == NULL) return NULL;

	/* set the battery # path */
	pcapacity[BAT_PATH_NUM_INDEX] = b_num;
	fcapacity = fopen(pcapacity, "r");
	free(pcapacity);

	/* read the battery capacity */
	capacity = read_file(fcapacity);
	fclose(fcapacity);

	return capacity;
}

char *battery_get_status (int b_num)
{
	char *pstatus; /* modified path for status file */
	FILE *fstatus; /* file handle for status file */
	char *status;  /* contents of status file */

	/* setup files for reading status */
	pstatus = strdup(BAT_STATUS_PATH);
	/* fail if strdup fails */
	if (pstatus == NULL) return NULL;
	/* set the battery # path */
	pstatus[BAT_PATH_NUM_INDEX] = b_num;
	fstatus = fopen(pstatus, "r");
	free(pstatus);

	/* read the battery status */
	status = read_file(fstatus);
	fclose(fstatus);

	return status;
}
