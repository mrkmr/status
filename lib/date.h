/* returns the date - month-day */
char *date_date (const time_t now);

/* returns the time - hour:minute:seconds */
char *date_time (const time_t now);
