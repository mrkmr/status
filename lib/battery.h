/* returns battery levels
 *   b_num: the battery number to try, e.g. '0' for "BAT0"
 */
char *battery (char b_num);

/* returns battery capacity [0-100]
 *   b_num: the battery number.
 */
char *battery_get_capacity (int b_num);

/* returns battery status (Charging, Discharging, Unknown, Full)
 *   b_num: the battery number.
 */
char *battery_get_status (int b_num);
