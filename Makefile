# status bar

CC=cc
CFLAGS = -O2 -Wall -Wextra -Werror -ansi -pedantic -pedantic-errors\
         -Wmissing-prototypes -Wstrict-prototypes -Wold-style-definition \
         -fdiagnostics-color=always

OBJ_DIR = obj
SRC_DIR = lib

BIN = status
LIB = common battery date
SRC = $(LIB:%=$(SRC_DIR)/%.c)
OBJ = $(LIB:%=$(OBJ_DIR)/%.o)

all: build test
debug: build-debug test-debug

status: $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $@.c -o $@

# release targets
build: $(BIN)
test:
	./status

# debug targets
build-debug: CFLAGS += -ggdb
build-debug: $(BIN)
test-debug:
	valgrind --leak-check=full --track-origins=yes ./status 2>&1 | less

# cleanup
clean:
	rm -f $(BIN)
	rm -rf ${OBJ_DIR}

# general rule for building lib/%.c files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

# specific rules for lib files
${OBJ_DIR}/battery.o: CFLAGS += -D_XOPEN_SOURCE=500
